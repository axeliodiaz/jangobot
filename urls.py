# -*- coding: utf8
from django.conf.urls.defaults import patterns, include, url
from django.views.generic import ListView
#from django.views.generic.simple import direct_to_template
from django.views.generic import TemplateView
from bot.models import MasUno,Nicknames
from django.db.models import Count

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
     url(r'^nicknames', ListView.as_view(queryset=MasUno.objects.values('nick__nick').annotate(Count('nick')),
         context_object_name=u'masuno_list',
         template_name='frontend/nicknames.html',
         )
         ),
     url(r'^mensajes', ListView.as_view(queryset=MasUno.objects.values('mensaje__mensaje').annotate(Count('mensaje')),
         context_object_name=u'masuno_list',
         template_name='frontend/mensajes.html',
         )
        ),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
     (r'^.*$', TemplateView.as_view(template_name="frontend/index.html")),
)
