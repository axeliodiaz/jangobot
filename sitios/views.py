# Create your views here.
from django.http import Http404, HttpResponsePermanentRedirect

from sitios.utils import reverse_code

def redirect(request, code):
    pk = reverse_code(code)
    try:
        short = Shortener.objects.get(pk=pk)
        short.clicks = short.clicks + 1
        short.save()
        return HttpResponsePermanentRedirect(short.url)
    except:
        raise Http404
