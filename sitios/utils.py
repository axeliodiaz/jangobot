def generate_code(id, alphabet='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'):
    if id <= 0:
        code = alphabet[0]
    else:
        base = len(alphabet)
        chars = []
        while id:
            chars.append(alphabet[id % base])  
            id //= base
        chars.reverse()
        return ''.join(chars)

def reverse_code(code, alphabet='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'):
    base = len(alphabet)
    size = len(code)  
    id = 0  
    for i in range(0, size):
        id += alphabet.index(code[i]) * (base ** (size-i-1))  
    return id
