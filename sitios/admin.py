from django.contrib import admin
from sitios.models import *

class SitiosAdmin(admin.ModelAdmin):
    list_display = ['url','texto']
admin.site.register(Sitios,SitiosAdmin)

class DominiosAdmin(admin.ModelAdmin):
    list_display = ['dominios']
admin.site.register(Dominios,DominiosAdmin)

class ShortenerAdmin(admin.ModelAdmin):
    list_display = ['url','clicks']
admin.site.register(Shortener,ShortenerAdmin)

