from django.db import models
from bot.models import *

class Sitios(models.Model):
    url = models.URLField()
    texto = models.ForeignKey(Mensajes)
    class Meta:
        db_table = u'sitios'
        verbose_name_plural = u'sitios'

    def __unicode__(self):
        return "%s: %s"%(self.url, self.texto)

class Dominios(models.Model):
    dominios = models.CharField(max_length=10)
    class Meta:
        db_table = u'dominios'
        verbose_name_plural = 'dominios'

    def __unicode__(self):
        return "%s"%(self.dominios)

class Shortener(models.Model):
    url = models.URLField()
    clicks = models.IntegerField(default=0)
    class Meta:
        db_table = u'shortener'

    def __unicode__(self):
        return "%s"%(self.url)


