jangobot
========
Bot para canal IRC basado en Django (soporta Django 1.5.4)

Sincronizar la base de datos

    * python manage.py syncdb 

Cargar Fixtures
 
    * python manage.py loaddata sitios/fixtures/dominios.json

Para ejecutar el Bot se debe ejecutar usando el comando:

  DJANGO_SETTINGS_MODULE=settings python jango_bot.py
