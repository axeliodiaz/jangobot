#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
# Example program using ircbot.py.
#
# Joel Rosdahl <joel@rosdahl.net>

"""A simple example bot.

This is an example bot that uses the SingleServerIRCBot class from
ircbot.py.  The bot enters a channel and listens for commands in
private messages and channel traffic.  Commands in channel messages
are given by prefixing the text by the bot name followed by a colon.
It also responds to DCC CHAT invitations and echos data sent in such
sessions.

The known commands are:

    stats -- Prints some channel information.

    disconnect -- Disconnect the bot.  The bot will try to reconnect
                  after 60 seconds.

    die -- Let the bot cease to exist.

    dcc -- Let the bot invite you to a DCC CHAT connection.
"""

from ircbot import SingleServerIRCBot
from irclib import nm_to_n, nm_to_h, irc_lower, ip_numstr_to_quad, ip_quad_to_numstr,DEBUG
from django.core.management import setup_environ
import os
import settings
#from jangobot import settings
#Activando setiings de django
setup_environ(settings)
from django.db.utils import IntegrityError
from bot.models import *
from sitios.models import *
from django.core.mail import EmailMultiAlternatives
DEBUG = 1

dom=Dominios.objects.all()
lista_dominio = []
for dominio in dom:
    lista_dominio.append(dominio.dominios)

class JangoBot(SingleServerIRCBot):
    ultimoMsg=0
    def __init__(self, channel, nickname, server, port=6667):
        SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)

    def on_privmsg(self, c, e):
        nick = nm_to_n(e.source())
        mensaje=Mensajes()
        mensaje.mensaje="(privado) %s" %(e.arguments()[0])
        nickname=Nicknames.objects.get_or_create(nick=nick)[0]
        mensaje.nickname=nickname
        mensaje.save()
        c.privmsg(nick, "!Hey!. Sal de mi privado... Todo lo que me dices esta siendo registrado" )
        #self.do_command(e, e.arguments()[0])

    def on_op(self, c, e):
        c.oper(erlobus, vencert_priv)

    def masUno(self,msg):
        masuno=MasUno(nick=msg.nickname,mensaje=msg,)
        try:
            masuno.save()
        except IntegrityError:
            pass
        return

    def on_pubmsg(self, c, e):
        #generate_code(shortener.id)
        a = e.arguments()[0].split(":", 1)
        nick = nm_to_n(e.source())
        mensaje=Mensajes()
        mensaje.mensaje=str(e.arguments()[0])
        nickname=Nicknames.objects.get_or_create(nick=nick)[0]
        mensaje.nickname=nickname
        try:
            mensaje.save()
        except:
            print "No se pudo guardar por error de caracteres"
        # Revisar si es un link:
        cont_dom = 0
        domin = ''

        if self.channel == '#canaima':
            #saludos = ['como estan', 'hola', 'bien y tu', ':-X', ':-D', 'xd', 'donde estas']
            saludos = Saludos.objects.all()
            revisar_saludo = 0
            for saludo in saludos:
                if Mensajes.objects.filter(id=mensaje.id, mensaje__icontains=saludo.texto).exists():
                    revisar_saludo = 1
            if revisar_saludo == 1:
                c.privmsg(self.channel, u"%s: Esto es una sala de reporte de incidentes telemáticos. Limitate a hablar de otro tema en particular." %(nick))

            insultos= Insultos.objects.all()
            revisar_insulto = 0
            for insulto in insultos:
                if Mensajes.objects.filter(id=mensaje.id, mensaje__icontains=insulto.texto).exists():
                    revisar_insulto = 1
            if revisar_insulto == 1:
                c.privmsg(self.channel, u"%s: Esto es una sala de reporte de incidentes telemáticos. Por favor, ahorrate insultar o denigrar a otros." %(nick))
        if self.channel == '#vencert':
            insultos= Insultos.objects.all()
            revisar_insulto = 0
            for insulto in insultos:
                if Mensajes.objects.filter(id=mensaje.id, mensaje__icontains=insulto.texto).exists():
                    revisar_insulto = 1
            if revisar_insulto == 1:
                c.privmsg(self.channel, u"%s: Esto es un espacio de trabajo tranquilo. Por favor, ahorrate insultar o denigrar a otros. =/" %(nick))

        for dominio in lista_dominio:
            tiene_dominio = Mensajes.objects.filter(id=mensaje.id, mensaje__icontains=dominio)
            if tiene_dominio.exists():
                ultimo_dominio = tiene_dominio[0]
                cont_dom = 1

                domin = dominio

        if cont_dom == 1:
            sobrante_1 = ultimo_dominio.mensaje.split('http')[0]
            sobrante_2 = ultimo_dominio.mensaje.split('%s'%(domin))[1]
            url = ultimo_dominio.mensaje.strip('%s'%(sobrante_1)).strip('%s'%(sobrante_2))
            c.privmsg(self.channel, "%s: Eso es un dominio? como que si! se ha guardado %s en la base de datos ;)" %(nick,url))
            sitio = Sitios.objects.create(url=url, texto=mensaje)
            subject, from_email, to = 'Url Reportada por canal IRC #vencert', 'eliezerfot123@gmail.com', 'eliezerfot123@gmail.com'
            text_content = 'Url reportada en el canal IRC #vencert'
            html_content = '<b>Url reportada en el canal IRC #vencert</b><b>URL: </b>'+url+'<hr><b>Reportado por (nick):</b>'+nick+'<hr>Por favor revisar la incidencia'
            msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
            msg.attach_alternative(html_content, "text/html")
            msg.send()


        self.ultimoMsg=mensaje
        mensaje = Mensajes.objects.filter(id=mensaje.id, mensaje__icontains='+1')
        if mensaje.exists():
            c.privmsg(self.channel, "%s: Se ha contado tu voto." %(nick))
            self.masUno(mensaje[0])
        else:
            if len(a) > 1 and irc_lower(a[0]) == irc_lower(self.connection.get_nickname()):
                self.do_command(e, a[1].strip())
        return

    def on_dccmsg(self, c, e):
        c.privmsg("You said: " + e.arguments()[0])

    def on_dccchat(self, c, e):
        if len(e.arguments()) != 2:
            return
        args = e.arguments()[1].split()
        if len(args) == 4:
            try:
                address = ip_numstr_to_quad(args[2])
                port = int(args[3])
            except ValueError:
                return
            self.dcc_connect(address, port)

    def do_command(self, e, cmd):
        nick = nm_to_n(e.source())
        c = self.connection
        if cmd == "chao" and nick == "eliezerfot123":
            c.privmsg(self.channel, "Me corres? al cabo que ni queria estar aqui! -_-")
            self.disconnect()
        if cmd == "chao":
            c.privmsg(self.channel, "¡No me corras tu eres chevere! xD")
        elif cmd.__contains__('hola'):
            c.privmsg(self.channel, "Hoooolaaaa!!! ¿Que tal te encuentras?")
        elif cmd == "vete!":
            c.privmsg(self.channel, "Adiooos mundo crueeel =( buuhhh!!! :P ")
        elif cmd == "saluda":
            c.privmsg(self.channel, "Oh, perdonen todos mi descortesia... Como estan cuerda de patanes?")
        elif cmd == "bien":
            c.privmsg(self.channel, "¿Tienes alguna incidencia?")
        elif cmd == "si":
            c.privmsg(self.channel, "Adelante, soy todo oidos...")
        else:
            c.privmsg(self.channel, "%s: Que quieres decir con '%s'?" %(nick, cmd))
            '''
        elif cmd == "stats":
            for chname, chobj in self.channels.items():
                c.notice(nick, "--- Channel statistics ---")
                c.notice(nick, "Channel: " + chname)
                users = chobj.users()
                users.sort()
                c.notice(nick, "Users: " + ", ".join(users))
                opers = chobj.opers()
                opers.sort()
                c.notice(nick, "Opers: " + ", ".join(opers))
                voiced = chobj.voiced()
                voiced.sort()
                c.notice(nick, "Voiced: " + ", ".join(voiced))
        elif cmd == "dcc":
            dcc = self.dcc_listen()
            c.ctcp("DCC", nick, "CHAT chat %s %d" % (
                ip_quad_to_numstr(dcc.localaddress),
                dcc.localport))
        else:
            c.notice(nick, "Not understood: " + cmd)
            '''


def main():
    channel = settings.JANGOBOT['channel']

    nickname = settings.JANGOBOT['nick']
    print "Hola estoy iniciado el servidor"
    print "......."

    bot = JangoBot(channel, nickname, settings.JANGOBOT['server'], settings.JANGOBOT['port'],)
    bot.start()

if __name__ == "__main__":
    main()
