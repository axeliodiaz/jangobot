
from django.contrib import admin
from models import *

class MasUnosAdmin(admin.ModelAdmin):
    list_display=('nick','mensaje',)

admin.site.register(MasUno,MasUnosAdmin)
admin.site.register([Mensajes,Nicknames])

class InsultosAdmin(admin.ModelAdmin):
    list_display= ['texto']
admin.site.register(Insultos,InsultosAdmin)
