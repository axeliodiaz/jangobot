from django.db import models

class Nicknames(models.Model):
    nick= models.CharField(max_length=20)
    class Meta:
        db_table = u'nicknames'
        verbose_name_plural=u'Nicknames'
    def __unicode__(self):
        return self.nick

class Mensajes(models.Model):
    mensaje= models.CharField(max_length=500)
    nickname= models.ForeignKey(Nicknames)
    class Meta:
        db_table = u'mensajes'
        verbose_name_plural=u'Mensajes'
    def __unicode__(self):
        return self.mensaje

class MasUno(models.Model):
    nick= models.ForeignKey(Nicknames,related_name='origen')
    mensaje= models.ForeignKey(Mensajes)

    class Meta:
        db_table = u'masuno'
        unique_together=('nick','mensaje')
    def cuantos(self,nick):
        return self.nick.get(nick=nick).length

    def __unicode__(self):
        return "%s: %s"%(self.nick.nick,self.mensaje.mensaje,)


class Insultos(models.Model):
    texto = models.CharField(max_length=100)
    class Meta:
        db_table = u'insultos'
        verbose_name = u'insultos'
    def __unicode__(self):
        return u'%s' %(self.texto)

